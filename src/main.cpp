#include <SPI.h>
#include "RF24.h"
#include <Servo.h>
#define RECEIVER true

#if RECEIVER
Servo ch1,ch2,ch3,ch4;
#endif

typedef struct payload{
    uint8_t ch1;
    uint8_t ch2;
    uint8_t ch3;
    uint8_t ch4;
} payload_t;

payload_t current_payload;

RF24 radio(8,7);
byte address[6] = {"node1"};


void setup() {
#if RECEIVER
    ch1.attach(5);
    ch2.attach(6);
    ch3.attach(3);
    ch4.attach(4);

    ch2.write(0);
    delay(1000);
    ch2.write(0);
    delay(1000);
    ch2.write(7);
    delay(1000);
    ch2.write(9);
    delay(1000);
    ch2.write(15);
    delay(1000);

#endif

    Serial.begin(115200);
    if(!radio.begin())
        Serial.println("ERROR while connecting rf24");
    else
        Serial.println("OK while connecting rf24");

    radio.setChannel(96);
    radio.setPALevel(RF24_PA_MAX);
    radio.setDataRate(RF24_250KBPS);
    radio.setAutoAck(1);                     // Ensure autoACK is enabled
    radio.setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries

    radio.setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
#if RECEIVER
    radio.openReadingPipe(1,address);
    radio.startListening();
#else
    radio.openWritingPipe(address);
    radio.stopListening();
#endif

}

void loop() {

#if !RECEIVER
    current_payload.ch1 = map(analogRead(A1),300,700,180,0);
    current_payload.ch2 = map(analogRead(A2),300,700,180,0);
    current_payload.ch3 = map(analogRead(A3),700,300,180,0);
    current_payload.ch4 = map(analogRead(A4),300,700,180,0);

    if(current_payload.ch1 < 15)
        current_payload.ch1 = 0;
    if(current_payload.ch2 < 15)
        current_payload.ch2 = 0;
    if(current_payload.ch3 < 15)
        current_payload.ch3 = 0;
    if(current_payload.ch4 < 15)
        current_payload.ch4 = 0;

    if(current_payload.ch1 > 165)
        current_payload.ch1 = 179;
    if(current_payload.ch2 > 165)
        current_payload.ch2 = 179;
    if(current_payload.ch3 > 165)
        current_payload.ch3 = 179;
    if(current_payload.ch4 > 165)
        current_payload.ch4 = 179;

    Serial.print(current_payload.ch1);
    Serial.print("  ");
    Serial.print(current_payload.ch2);
    Serial.print("  ");
    Serial.print(current_payload.ch3);
    Serial.print("  ");
    Serial.print(current_payload.ch4);

    if(!radio.write(&current_payload, sizeof(payload_t)))
        Serial.println(" FAIL");
    else
        Serial.println(" OK");

    delay(50);
#elif RECEIVER
    static unsigned long last_message_delay = 0;
    if(radio.available()){
        while (radio.available()) {                                   // While there is data ready
            radio.read(&current_payload, sizeof(payload_t));             // Get the payload
        }
        Serial.print(current_payload.ch1);
        Serial.print("  ");
        Serial.print(current_payload.ch2);
        Serial.print("  ");
        Serial.print(current_payload.ch3);
        Serial.print("  ");
        Serial.println(current_payload.ch4);
        last_message_delay = millis();
    }
    if (millis() - last_message_delay > 3000) {
        current_payload.ch2 = 0;
        current_payload.ch3 = 120;

    }
    ch1.write(current_payload.ch1);
    ch2.write(current_payload.ch2);
    ch3.write(current_payload.ch3);
    ch4.write(current_payload.ch4);
#endif
} // Loop
